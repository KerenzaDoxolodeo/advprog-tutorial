package tallycounter;

public class SynchronizedTallyCounter {
    private int counter = 0;

    public synchronized void increment() {
        counter++;
    }

    public void decrement() {
        counter--;
    }

    public int value() {
        return counter;
    }
}
