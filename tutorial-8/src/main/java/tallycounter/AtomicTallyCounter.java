package tallycounter;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTallyCounter {
    private AtomicInteger tally = new AtomicInteger(0);

    public void increment() {
        int count = tally.addAndGet(1);
    }

    public void decrement() {
        int count = tally.addAndGet(-1);
    }

    public int value() {
        return tally.get();
    }
}
