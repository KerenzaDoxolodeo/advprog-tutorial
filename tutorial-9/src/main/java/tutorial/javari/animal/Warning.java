package tutorial.javari.animal;

public class Warning {

    private String message = null;

    public Warning(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}
