package tutorial.javari;

import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;
import tutorial.javari.animal.Warning;

import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class JavariController {
	private ArrayList<Animal> daftarBinatang;
	private AtomicLong counter;
	private String csvFile = "data.csv";

	public synchronized void write(){
	    String line = "";
	    for(Animal animal: daftarBinatang){
            Integer id = animal.getId();
            String type = animal.getType();
            String name = animal.getName();
            String gender = animal.getGender().name();
            Double length = animal.getLength();
            Double weight = animal.getWeight();
            String condition = animal.getCondition().name();
            line += id+","+type+","+name+","+gender+","+length+","+weight+","+condition+"\n";
        }

        BufferedWriter bw = null;

        try {
            bw = new BufferedWriter(new FileWriter(csvFile));
            bw.write(line);
            bw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public void read() {
    	daftarBinatang = new ArrayList<Animal>();

        BufferedReader br = null;
        String line;
        try {
        	long highestIndex = 0;
            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {
                String[] data = line.split(",");
                Integer id = Integer.parseInt(data[0]);
                String type = data[1];
                String name = data[2];
                Gender gender = Gender.parseGender(data[3]);
                Double length = Double.parseDouble(data[4]);
                Double weight = Double.parseDouble(data[5]);
                Condition condition = Condition.parseCondition(data[6]);
                Animal animal = new Animal(id,type,name,gender,length,weight,condition);
                daftarBinatang.add(animal);
            }
       		br.close();
       		counter = new AtomicLong(highestIndex);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
    	}
	}

    @GetMapping("/javari")
    Object requestListBinatang() {
        if(this.daftarBinatang == null || this.daftarBinatang.size() == 0) {
            read();
        }
        return this.daftarBinatang;
    }

    @GetMapping("/javari/{idAnimal}")
    Object searchBinatang(@PathVariable Integer idAnimal){
        if(this.daftarBinatang == null) {
            read();
        }
        Animal animalYangDicari = null;
        for(Animal animal:this.daftarBinatang){
            if(animal.getId() == idAnimal){
                animalYangDicari =  animal;
            }
        }
        if(animalYangDicari == null){
            System.out.println("DING");
            return new Warning("Animal yang ada cari tidak dapat ditemukan");
        }
        return animalYangDicari;
    }

    @DeleteMapping("/javari/{idAnimal}")
    Object deleteBinatang(@PathVariable Integer idAnimal){
        if(this.daftarBinatang == null) {
            read();
        }
        Animal yangDibuang = null;
        for(Animal animal : this.daftarBinatang){
            if(animal.getId() == idAnimal){
                yangDibuang = animal;
            }
        }
        if(yangDibuang == null){
            return new Warning("File yang dibuang tidak ditemukan");
        }
        this.daftarBinatang.remove(yangDibuang);
        write();
        return yangDibuang;
    }

    @PostMapping("/javari/{idAnimal}")
    void addBinatang(@PathVariable Integer idAnimal, @RequestBody Animal input){
        if(this.daftarBinatang == null) {
            read();
        }
        daftarBinatang.add(input);
        write();
    }


}
