package tutorial.javari;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;


import static org.assertj.core.internal.bytebuddy.matcher.ElementMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class JavariControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetIdNumberOne() throws Exception {
        this.mockMvc.perform(get("/javari/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value("1"));
    }

    @Test
    public void testReturnWarningOnBadGet() throws Exception {
        this.mockMvc.perform(get("/javari/20"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message").
                        value("Animal yang ada cari tidak dapat ditemukan"));
    }
}