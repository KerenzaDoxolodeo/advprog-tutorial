package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import org.junit.Test;

public class EggplantTest {
    private Eggplant eggplant;

    @Test
    public void toStringEggplant() {

        eggplant = new Eggplant();
        assertEquals("Eggplant",eggplant.toString());

    }
}
