package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import org.junit.Test;

public class FreshClamsTest {
    private FreshClams freshClams;

    @Test
    public void toStringFreshClams() {

        freshClams = new FreshClams();
        assertEquals("Fresh Clams from Long Island Sound",freshClams.toString());

    }
}
