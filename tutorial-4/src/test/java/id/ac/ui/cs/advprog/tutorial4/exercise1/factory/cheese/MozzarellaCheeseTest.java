package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.MozzarellaCheese;
import org.junit.Test;

public class MozzarellaCheeseTest {
    private MozzarellaCheese mozzarellaCheese;

    @Test
    public void toStringMozarellaCheese() {

        mozzarellaCheese = new MozzarellaCheese();
        assertEquals("Shredded Mozzarella",mozzarellaCheese.toString());

    }
}
