package id.ac.ui.cs.advprog.tutorial4.exercise1.dough;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.NoCrustDough;
import org.junit.Test;

public class NoCrustDoughTest {
    private NoCrustDough noCrustDough;

    @Test
    public void toStringNoCrustDough() {

        noCrustDough = new NoCrustDough();
        assertEquals("No Crust Dough",noCrustDough.toString());

    }
}
