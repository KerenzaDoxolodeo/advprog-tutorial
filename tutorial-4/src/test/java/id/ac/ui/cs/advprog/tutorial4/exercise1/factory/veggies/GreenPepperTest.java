package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.GreenPepper;
import org.junit.Test;

public class GreenPepperTest {
    private GreenPepper greenPepper;

    @Test
    public void toStringGreenPepper() {

        greenPepper = new GreenPepper();
        assertEquals("Green Pepper",greenPepper.toString());

    }
}
