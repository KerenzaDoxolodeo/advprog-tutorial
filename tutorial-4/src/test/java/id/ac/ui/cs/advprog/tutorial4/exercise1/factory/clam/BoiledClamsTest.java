package id.ac.ui.cs.advprog.tutorial4.exercise1.clam;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.BoiledClams;
import org.junit.Test;

public class BoiledClamsTest {
    private BoiledClams boiledClams;

    @Test
    public void toStringBoiledClams() {

        boiledClams = new BoiledClams();
        assertEquals("Boiled Clams from Ancol",boiledClams.toString());

    }
}
