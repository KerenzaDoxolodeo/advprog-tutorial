package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.ChiliSauce;
import org.junit.Test;

public class ChiliSauceTest {
    private ChiliSauce chiliSauce;

    @Test
    public void toStringChiliSauce() {

        chiliSauce = new ChiliSauce();
        assertEquals("Chili sauce. It's hot!",chiliSauce.toString());

    }
}
