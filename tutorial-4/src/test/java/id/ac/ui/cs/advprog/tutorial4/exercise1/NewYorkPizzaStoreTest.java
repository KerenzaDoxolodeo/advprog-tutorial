package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.NewYorkPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaStoreTest {

    private NewYorkPizzaStore newYorkPizzaStore;

    @Before
    public void setUp() {
        newYorkPizzaStore = new NewYorkPizzaStore();
    }

    @Test
    public void testNewYorkReturnsNullIfBadInput() {
        Pizza result = newYorkPizzaStore.createPizza("Bad result");
        assertEquals(result,null);
    }

    @Test
    public void testNewYorkCheesePizza() {
        Pizza result = newYorkPizzaStore.createPizza("cheese");
        assertEquals(result.getName(),"New York Style Cheese Pizza");
    }

    @Test
    public void testDepokClamPizza() {
        Pizza result = newYorkPizzaStore.createPizza("clam");
        assertEquals(result.getName(),"New York Style Clam Pizza");
    }

    @Test
    public void testDepokVeggiePizza() {
        Pizza result = newYorkPizzaStore.createPizza("veggie");
        assertEquals(result.getName(),"New York Style Veggie Pizza");
    }
}