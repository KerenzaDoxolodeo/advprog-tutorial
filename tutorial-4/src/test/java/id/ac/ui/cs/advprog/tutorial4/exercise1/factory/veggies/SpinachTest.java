package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import org.junit.Test;

public class SpinachTest {
    private Spinach spinach;

    @Test
    public void toStringSpinach() {

        spinach = new Spinach();
        assertEquals("Spinach",spinach.toString());

    }
}
