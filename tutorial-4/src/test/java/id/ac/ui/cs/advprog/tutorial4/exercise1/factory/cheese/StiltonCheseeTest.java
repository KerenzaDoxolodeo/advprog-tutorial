package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.StiltonCheese;
import org.junit.Test;

public class StiltonCheseeTest {
    private StiltonCheese stiltonCheese;

    @Test
    public void toStringStiltonCheese() {

        stiltonCheese = new StiltonCheese();
        assertEquals("Stilton Cheese",stiltonCheese.toString());

    }
}
