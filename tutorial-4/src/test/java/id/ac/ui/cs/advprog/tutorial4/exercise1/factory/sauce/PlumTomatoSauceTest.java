package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.PlumTomatoSauce;
import org.junit.Test;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plumTomatoSauce;

    @Test
    public void toStringPlumTomatoSauce() {

        plumTomatoSauce = new PlumTomatoSauce();
        assertEquals("Tomato sauce with plum tomatoes",plumTomatoSauce.toString());

    }
}
