package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.DepokPizzaStore;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;
import org.junit.Before;
import org.junit.Test;

public class DepokPizzaStoreTest {

    private DepokPizzaStore depokPizzaStore;

    @Before
    public void setUp() {
        depokPizzaStore = new DepokPizzaStore();
    }

    @Test
    public void testDepokReturnsNullIfBadInput() {
        Pizza result = depokPizzaStore.createPizza("Bad result");
        assertEquals(result,null);
    }

    @Test
    public void testDepokCheesePizza() {
        Pizza result = depokPizzaStore.createPizza("cheese");
        assertEquals(result.getName(),"Depok Style Cheese Pizza");
    }

    @Test
    public void testDepokClamPizza() {
        Pizza result = depokPizzaStore.createPizza("clam");
        assertEquals(result.getName(),"Depok Style Clam Pizza");
    }

    @Test
    public void testDepokVeggiePizza() {
        Pizza result = depokPizzaStore.createPizza("veggie");
        assertEquals(result.getName(),"Depok Style Veggie Pizza");
    }
}