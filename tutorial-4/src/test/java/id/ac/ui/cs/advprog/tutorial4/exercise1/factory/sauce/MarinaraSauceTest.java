package id.ac.ui.cs.advprog.tutorial4.exercise1.sauce;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import org.junit.Test;

public class MarinaraSauceTest {
    private MarinaraSauce marinaraSauce;

    @Test
    public void toStringMarinaraSauce() {

        marinaraSauce = new MarinaraSauce();
        assertEquals("Marinara Sauce",marinaraSauce.toString());

    }
}
