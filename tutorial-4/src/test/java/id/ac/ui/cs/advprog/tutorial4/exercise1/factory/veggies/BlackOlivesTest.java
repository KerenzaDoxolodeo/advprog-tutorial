package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import org.junit.Test;

public class BlackOlivesTest {
    private BlackOlives blackOlives;

    @Test
    public void toStringblackOlives() {

        blackOlives = new BlackOlives();
        assertEquals("Black Olives",blackOlives.toString());

    }
}
