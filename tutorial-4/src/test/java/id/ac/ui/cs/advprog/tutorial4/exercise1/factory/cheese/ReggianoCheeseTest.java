package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import org.junit.Test;

public class ReggianoCheeseTest {
    private ReggianoCheese reggianoCheese;

    @Test
    public void toStringReggianoCheese() {

        reggianoCheese = new ReggianoCheese();
        assertEquals("Reggiano Cheese",reggianoCheese.toString());

    }
}
