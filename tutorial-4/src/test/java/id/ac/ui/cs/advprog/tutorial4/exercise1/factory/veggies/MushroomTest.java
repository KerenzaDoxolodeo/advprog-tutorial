package id.ac.ui.cs.advprog.tutorial4.exercise1.veggies;

import static org.junit.Assert.assertEquals;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import org.junit.Test;

public class MushroomTest {
    private Mushroom mushroom;

    @Test
    public void toStringMushroom() {

        mushroom = new Mushroom();
        assertEquals("Mushrooms",mushroom.toString());

    }
}
