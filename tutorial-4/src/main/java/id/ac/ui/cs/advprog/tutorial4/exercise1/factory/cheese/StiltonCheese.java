package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class StiltonCheese implements Cheese {

    public String toString() {
        return "Stilton Cheese";
    }
}
