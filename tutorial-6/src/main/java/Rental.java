class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double getThisAmount() {
        double thisAmount = 0;
        switch (movie.getPriceCode()) {
            case Movie.REGULAR:
                thisAmount = 2;
                if (daysRented > 2) {
                    return thisAmount + (daysRented - 2) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                return daysRented * 3;
            case Movie.CHILDREN:
                thisAmount = 1.5;
                if (daysRented > 3) {
                    return thisAmount + (daysRented - 3) * 1.5;
                }
                break;
            default:
                break;
        }
        return thisAmount;
    }

    public int getFrequentRenterPoints() {
        // Add bonus for a two day new release rental
        if ((movie.getPriceCode() == Movie.NEW_RELEASE) 
            && daysRented > 1) {
            return 2;
        }
        return 1;
    }
}