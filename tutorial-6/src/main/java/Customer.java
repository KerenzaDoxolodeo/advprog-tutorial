import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String htmlStatement() {
        String result = "<p>Rental Record for "
                        + name
                        + "</p>";

        Iterator<Rental> iterator = rentals.iterator();

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += "<p>" + each.getMovie().getTitle()
                    + " : "
                    + String.valueOf(each.getThisAmount())
                    + "</p>";

        }

        // Add footer lines
        result += "<p>Amount owed is "
                + String.valueOf(getTotalAmount(rentals.iterator()))
                + "</p>";
        result += "<p>You earned "
                + String.valueOf(getFrequentRenterPoints(rentals.iterator()))
                + " frequent renter points</p>";

        return result;
    }

    public String statement() {
        String result = "Rental Record for "
                        + name
                        + "\n";

        Iterator<Rental> iterator = rentals.iterator();

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            result += "\t"
                    + each.getMovie().getTitle()
                    + "\t"
                    + String.valueOf(each.getThisAmount())
                    + "\n";

        }

        // Add footer lines
        result += "Amount owed is "
                + String.valueOf(getTotalAmount(rentals.iterator()))
                + "\n";
        result += "You earned " + String.valueOf(getFrequentRenterPoints(rentals.iterator()))
               + " frequent renter points";

        return result;
    }

    private double getTotalAmount(Iterator<Rental> iterator) {
        double totalAmount = 0;
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            totalAmount += each.getThisAmount();
        }
        return totalAmount;
    }

    private double getFrequentRenterPoints(Iterator<Rental> iterator) {
        double frequentRenterPoints = 0;
        while (iterator.hasNext()) {
            Rental each = iterator.next();
            frequentRenterPoints += each.getFrequentRenterPoints();
        }
        return frequentRenterPoints;
    }
}