import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class RentalTest {

    private Movie movie;
    private Rental rent;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }

    @Test
    public void getMovie() {
        assertEquals(movie, rent.getMovie());
    }

    @Test
    public void getDaysRented() {
        assertEquals(3, rent.getDaysRented());
    }

    @Test
    public void getFrequentRenterPoints() {
        assertEquals(1,rent.getFrequentRenterPoints());
        rent.getMovie().setPriceCode(Movie.NEW_RELEASE);
        assertEquals(2,rent.getFrequentRenterPoints());
    }

    @Test
    public void getThisAmount() {
        assertTrue(3.5 == rent.getThisAmount());
        rent.getMovie().setPriceCode(Movie.NEW_RELEASE);
        assertTrue(9 == rent.getThisAmount());
        rent.getMovie().setPriceCode(Movie.CHILDREN);
        Rental rent2 = new Rental(rent.getMovie(),4);
        assertTrue(rent2.getThisAmount() == 3);
    }

}