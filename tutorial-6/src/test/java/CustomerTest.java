import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class CustomerTest {

    private Customer customer;
    private Movie movie;
    private Rental rent;

    @Before
    public void setUp() {
        customer = new Customer("Alice");
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
        rent = new Rental(movie, 3);
    }

    @Test
    public void getName() {
        assertEquals("Alice", customer.getName());
    }

    @Test
    public void statementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("You earned 1.0 frequent renter points"));
    }

    @Test
    public void htmlStatementWithSingleMovie() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("</p>");
        System.out.println(result);
        System.out.println(lines.length);
        assertEquals(4, lines.length);
        assertTrue(result.contains("Amount owed is 3.5"));
        assertTrue(result.contains("You earned 1.0 frequent renter points"));
    }

    public void statementWithMultipleMovies() {
        customer.addRental(rent);

        Movie movie2 = new Movie("Film baru",Movie.NEW_RELEASE);
        customer.addRental(rent);
        String result = customer.statement();
        String[] lines = result.split("\n");

        assertEquals(8, lines.length);
        assertTrue(result.contains("You earned 3.0 frequent renter points"));

    }

    public void htmlStatementWithMultipleMovies() {
        customer.addRental(rent);

        String result = customer.htmlStatement();
        String[] lines = result.split("</p>");

        assertEquals(8, lines.length);
        assertTrue(result.contains("You earned 3.0 frequent renter points"));

    }
}