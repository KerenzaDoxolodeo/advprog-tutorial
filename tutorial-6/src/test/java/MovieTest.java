import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Objects;
import org.junit.Before;
import org.junit.Test;


public class MovieTest {

    private Movie movie;

    @Before
    public void setUp() {
        movie = new Movie("Who Killed Captain Alex?", Movie.REGULAR);
    }


    @Test
    public void getTitle() {
        assertEquals("Who Killed Captain Alex?", movie.getTitle());
    }

    @Test
    public void setTitle() {
        movie.setTitle("Bad Black");

        assertEquals("Bad Black", movie.getTitle());
    }

    @Test
    public void getPriceCode() {
        assertEquals(Movie.REGULAR, movie.getPriceCode());
    }

    @Test
    public void setPriceCode() {
        movie.setPriceCode(Movie.CHILDREN);
        assertEquals(Movie.CHILDREN, movie.getPriceCode());
    }

    @Test
    public void equals() {
        assertTrue(movie.equals(movie));
        assertTrue(!movie.equals(null));
        Movie movie2 = new Movie(movie.getTitle(),movie.getPriceCode());
        assertTrue(movie.equals(movie2));
    }

    @Test
    public void testHashCode() {
        assertEquals(Objects.hash(movie.getTitle(),movie.getPriceCode()),
                movie.hashCode());
    }
}