package sorting;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SortAndSearchTest {
    //TODO Implement, apply your test cases here

    @Test
    public void testSort(){
        int array[] = {5,9,12,0,-5};
        Sorter.slowSort(array);
        for(int I=0; I< array.length; I++){
            System.out.println(array[I]);
        }
        for(int I=0; I< array.length-1;I++){
            assertTrue(array[I] <= array[I+1]);
        }
    }

    @Test
    public void testFinder(){
        int array[] = {5,9,12,0,-5};
        int result = Finder.slowSearch(array,12);
        assertTrue(result == 12);
        result = Finder.slowSearch(array,50);
        assertTrue(result == -1);

    }

}
