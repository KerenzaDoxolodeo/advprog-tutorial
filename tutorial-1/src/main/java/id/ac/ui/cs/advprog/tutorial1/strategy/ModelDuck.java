package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    public ModelDuck() {
    	this.setQuackBehavior(new Quack());
       	this.setFlyBehavior(new FlyNoWay());
    }
    public void display() {
        System.out.println("I’m a model duck");
    }

    public void swim(){
    	System.out.println("Swim");
    }
}