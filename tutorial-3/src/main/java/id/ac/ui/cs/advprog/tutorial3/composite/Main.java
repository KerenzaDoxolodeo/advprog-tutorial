package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.*;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

public class Main{
	public static void main(String args[]){
		Company company = new Company();
		company.addEmployee(new Ceo("Luffy", 500000.00));
		company.addEmployee(new Cto("Zorro", 320000.00));
		System.out.println(company.getNetSalaries());
	}
}