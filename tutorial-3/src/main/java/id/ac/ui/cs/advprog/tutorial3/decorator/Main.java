package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;

public class Main{
	public static void main(String args[]){
		Food food = new ThinBunBurger();
		System.out.println(food.cost());
		food = new BeefMeat(food);
		System.out.println(food.cost());
	}
}